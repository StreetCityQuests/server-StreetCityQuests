package com.bdshadow.streetcityquests.service;

import com.bdshadow.streetcityquests.domain.User;
import com.bdshadow.streetcityquests.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Dmitrii Bocharov <bdshadow@gmail.com>
 */
@Service
public class UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Transactional(readOnly = true)
  public User findUserById(String id) {
    return userRepository.findById(id);
  }
}
