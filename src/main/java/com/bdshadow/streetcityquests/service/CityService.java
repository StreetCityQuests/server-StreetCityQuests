package com.bdshadow.streetcityquests.service;

import com.bdshadow.streetcityquests.domain.City;
import com.bdshadow.streetcityquests.repository.CityRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CityService {

  @Autowired
  private CityRepository cityRepository;

  @Transactional(readOnly = true)
  public List<City> getAllCities() {
    return cityRepository.findAll();
  }
}
