package com.bdshadow.streetcityquests.repository;

import com.bdshadow.streetcityquests.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
  User findByEmail(String email);
  User findById(String id);
  User findByLogin(String login);
}
