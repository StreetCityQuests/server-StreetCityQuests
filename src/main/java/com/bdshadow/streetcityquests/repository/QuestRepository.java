package com.bdshadow.streetcityquests.repository;

import com.bdshadow.streetcityquests.domain.Quest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestRepository extends JpaRepository<Quest, Integer> {

}
