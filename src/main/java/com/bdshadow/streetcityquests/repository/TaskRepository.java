package com.bdshadow.streetcityquests.repository;

import com.bdshadow.streetcityquests.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Integer> {

}
