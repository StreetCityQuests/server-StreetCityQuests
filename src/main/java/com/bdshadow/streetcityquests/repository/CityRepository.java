package com.bdshadow.streetcityquests.repository;

import com.bdshadow.streetcityquests.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Integer> {

}
