package com.bdshadow.streetcityquests.security;

import com.bdshadow.streetcityquests.domain.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * @author Dmitrii Bocharov <bdshadow@gmail.com>
 */
@Component
public class JwtTokenProvider {

  private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

  public String generateToken(Authentication authentication) throws FirebaseAuthException {
    User userPrincipal = (User) authentication.getPrincipal();
    return FirebaseAuth.getInstance().createCustomToken(userPrincipal.getId());
  }

  /**
   * @return uid
   */
  public String validateToken(String idToken) throws FirebaseAuthException {
    FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
    return decodedToken.getUid();
  }
}
