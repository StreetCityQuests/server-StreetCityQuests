package com.bdshadow.streetcityquests;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.util.ResourceUtils;

@SpringBootApplication
public class StreetcityquestsApplication {

  public static void main(String[] args) {
    SpringApplication.run(StreetcityquestsApplication.class, args);
  }

  @EventListener(ApplicationReadyEvent.class)
  public void initFirebase() throws IOException {
    FileInputStream serviceAccount = new FileInputStream(ResourceUtils.getFile("classpath:serviceAccountKey.json"));

    FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .build();

    FirebaseApp.initializeApp(options);
  }

}

