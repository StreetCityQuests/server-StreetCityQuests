package com.bdshadow.streetcityquests.controller;


import com.bdshadow.streetcityquests.domain.City;
import com.bdshadow.streetcityquests.service.CityService;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CityController {

  @Autowired
  private CityService cityService;

  @RequestMapping("/cities")
  public List<City> getAllCities() {
    return cityService.getAllCities();
  }

}
