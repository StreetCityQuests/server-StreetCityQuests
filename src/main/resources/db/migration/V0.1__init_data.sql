insert into city(name, image) values ('Brno', '/images/Brno/%s/brno-main.jpg');
insert into city(name, image) values ('Prague', '/images/Prague/%s/prague-main.jpg');

insert into quest(name, description, city) values ('The first quest in Brno', 'Description of the first quest in Brno', 1);
insert into quest(name, description, city) values ('The second quest in Brno', 'Description of the second quest in Brno', 2);
insert into quest(name, description, city) values ('The first quest in Prague', 'Description of the first quest in Prague', 2);


insert into task(quest, question, description) values (1, 'What is your name?', 'Go there and answer to this question');
insert into task(quest, question, description) values (1, 'What is your surname?', 'Go here and answer to this question');
insert into task(quest, question, description) values (1, 'What is your age?', 'Don''say anything... let me guess');