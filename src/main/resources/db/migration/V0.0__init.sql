create table city (
  id serial primary key,
  name varchar(30) not null,
  image varchar(100)
);

create table quest (
  id bigserial primary key,
  name varchar(100) not null,
  description varchar(1000),
  city integer references city,
  image varchar(100)
);

create table task (
  id bigserial primary key,
  quest bigint references quest,
  question varchar(300) not null,
  description varchar(1000),
  image varchar(100)
);

create table person (
  id varchar(100) primary key,
  email varchar(254) not null unique,
  login varchar(254),
  created timestamp with time zone not null,
  photo varchar(100)
)

